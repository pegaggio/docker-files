# run this script AFTER initial build and download has completed!
echo "starting container..."
docker-compose up -d
#echo "sleeping 60 seconds..."
#sleep 60
echo "updating file permissions"
docker exec kz-csgo chmod -R 777 /home/steam
echo "copying files"
echo "(1) copying kz plugins to container"
docker cp ./kz-plugins/addons/* kz-csgo:/home/steam/csgo/csgo/addons
docker cp ./kz-plugins/maps/* kz-csgo:/home/steam/csgo/csgo/maps
docker cp ./kz-plugins/materials/* kz-csgo:/home/steam/csgo/csgo/materials
docker cp ./kz-plugins/sound/* kz-csgo:/home/steam/csgo/csgo/sound
docker cp ./kz-plugins/cfg/* kz-csgo:/home/steam/csgo/csgo/cfg
echo "(2) copying server.cfg to container"
docker cp ./configs/server.cfg kz-csgo:/home/steam/csgo/csgo/cfg/server.cfg
echo "(3) deleting databases.cfg"
docker exec kz-csgo rm /home/steam/csgo/csgo/addons/sourcemod/configs/databases.cfg
echo "(4) copying new databases config"
docker cp ./configs/databases.cfg kz-csgo:/home/steam/csgo/csgo/addons/sourcemod/configs/database.cfg
echo "(5) copying workshop maps configuration"
docker cp ./configs/subscribed_file_ids.txt kz-csgo:/home/steam/csgo/csgo/subscribed_file_ids.txt
echo "done!"