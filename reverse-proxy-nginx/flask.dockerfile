# alpine with py3.9 - version application written in
FROM python:3.9-alpine

# expose port 5000 for connection to nginx
EXPOSE 5000

# create a directory to run the app in
WORKDIR /app
RUN cd /app

# copy application files

# run pip on requirements.txt

CMD 